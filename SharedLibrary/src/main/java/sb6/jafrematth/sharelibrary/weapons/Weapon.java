/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.sharelibrary.weapons;

import java.io.Serializable;
import java.util.ArrayList;
import sb6.jafrematth.sharedlibrary.enums.WeaponEnum;

/**
 *
 * @author jakob
 */
public abstract class Weapon implements Serializable {

    private ArrayList<WeaponEnum> actions;
    private int ammo;
    private float damage, projSpeed, projRange, projRadius;

    public Weapon(int ammo, float damage, float projSpeed, float projRange, float projRadius) {
        this.ammo = ammo;
        this.damage = damage;
        this.projSpeed = projSpeed;
        this.projRange = projRange;
        this.projRadius = projRadius;
        this.actions = new ArrayList<WeaponEnum>();
    }

    public ArrayList<WeaponEnum> getActions() {
        return actions;
    }

    public void setActions(ArrayList<WeaponEnum> actions) {
        this.actions = actions;
    }

    public int getAmmo() {
        return ammo;
    }

    public void setAmmo(int ammo) {
        this.ammo = ammo;
    }

    public float getDamage() {
        return damage;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }

    public float getProjSpeed() {
        return projSpeed;
    }

    public void setProjSpeed(float projSpeed) {
        this.projSpeed = projSpeed;
    }

    public float getProjRange() {
        return projRange;
    }

    public void setProjRange(float projRange) {
        this.projRange = projRange;
    }

    public float getProjRadius() {
        return projRadius;
    }

    public void setProjRadius(float projRadius) {
        this.projRadius = projRadius;
    }

}
