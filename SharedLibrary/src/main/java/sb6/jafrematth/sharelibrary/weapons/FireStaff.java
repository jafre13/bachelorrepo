/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.sharelibrary.weapons;

/**
 *
 * @author jakob
 */
public class FireStaff extends Weapon{
    
    public FireStaff(int ammo, float damage, float projSpeed, float projRange, float projRadius) {
        super(ammo, damage, projSpeed, projRange, projRadius);
    }
    
}
