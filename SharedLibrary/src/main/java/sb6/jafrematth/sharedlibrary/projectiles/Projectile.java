/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.sharedlibrary.projectiles;

import com.badlogic.gdx.math.Vector2;
import java.io.Serializable;

/**
 *
 * @author jakob
 */
public class Projectile implements Serializable{
    
    private Vector2 location;
    private Vector2 target;
    private Vector2 offSet;
    private float radius;
    private float speed;
    private String texture;
    private boolean clean;
    private int dmg;

    public Projectile(float x, float y, float desX, float desY, float radius, float speed) {
        this.location = new Vector2(x, y);
        this.target = new Vector2(desX, desY);
        this.offSet = calcOffSet(location, target);
        this.radius = radius;
        this.speed = speed;
        this.texture="FIREBALL.png";
        this.clean=false;
        this.dmg = 20;
    }

    private Vector2 calcOffSet(Vector2 start, Vector2 target){
        
        float distance = start.dst(target);
        Vector2 direction = target.sub(start).nor();

        Vector2 temp = new Vector2(start);
        
        temp.x+=direction.x*20;
        temp.y+=direction.y*20;
        return temp;
    }

    public int getDmg() {
        return dmg;
    }

    public void setDmg(int dmg) {
        this.dmg = dmg;
    }
    
    

    public Vector2 getOffSet() {
        return offSet;
    }
    
    
    
    public boolean isClean() {
        return clean;
    }

    public void setClean(boolean clean) {
        this.clean = clean;
    }

    
    public Vector2 getLocation() {
        return location;
    }

    public void setLocation(Vector2 location) {
        this.location = location;
    }

    public Vector2 getTarget() {
        return target;
    }

    public void setTarget(Vector2 target) {
        this.target = target;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public String getTexture() {
        return texture;
    }

    public void setTexture(String texture) {
        this.texture = texture;
    }
    
    
}
