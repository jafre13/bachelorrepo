/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.sharedlibrary.maps;

import java.io.Serializable;

/**
 *
 * @author jakob
 */
public class GameLevel implements Serializable {

    public String name;
    public int height;
    public int width;
    public int[][] objects;
   

    public GameLevel(String name, String tmxMapm, int height, int width) {
        this.name = name;
        this.height = height;
        this.width = width;
        this.objects = new int[width][height];
        

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int[][] getObjects() {
        return objects;
    }

    public void setObjects(int[][] objects) {
        this.objects = objects;
    }

}
