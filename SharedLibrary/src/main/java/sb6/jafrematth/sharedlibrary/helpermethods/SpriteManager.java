/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.sharedlibrary.helpermethods;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import org.openide.util.Lookup;

/**
 *
 * @author jakob
 */
public class SpriteManager {

    private static SpriteManager sm;
    ClassLoader cl = Lookup.getDefault().lookup(ClassLoader.class);

    private SpriteManager() {

    }

    public static SpriteManager getSpriteManager() {
        if (sm == null) {
            sm = new SpriteManager();
        }
        return sm;
    }

    public Texture getTexture(String url) {
        FileHandle f = new FileHandle(cl.getResource("assets/sprites/" + url).getPath());
        Texture tex = new Texture(f);
        return tex;
    }

    public Texture getMapTexture(String url) {
        FileHandle f = new FileHandle(cl.getResource("assets/maps/" + url).getPath());
        Texture tex = new Texture(f);
        return tex;
    }

    public Sprite getSprite(String url) {
        FileHandle f = new FileHandle(cl.getResource("assets/sprites/" + url).getPath());
        Texture tex = new Texture(f);
        Sprite sprite = new Sprite(tex);
        return sprite;
    }

    public String getUrl(String url) {
        return cl.getResource(url).getPath();
    }

    public Sprite getSpriteFromNumber(int number) {
        switch (number) {
            case 1:
                return getSprite("char.png");
            default:
                return null;
        }
    }
}
