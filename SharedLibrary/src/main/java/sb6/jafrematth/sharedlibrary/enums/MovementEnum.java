/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.sharedlibrary.enums;

/**
 *
 * @author jakob
 */
public enum MovementEnum {
    UP,DOWN,LEFT,RIGHT, UPRIGHT, DOWNRIGHT, UPLEFT, DOWNLEFT, STANDING;
}
