/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.sharedlibrary.entities;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;
import sb6.jafrematth.sharedlibrary.enums.MovementEnum;
import sb6.jafrematth.sharedlibrary.maps.GameLevel;
import sb6.jafrematth.sharedlibrary.projectiles.Projectile;

import sb6.jafrematth.sharelibrary.weapons.Weapon;

/**
 *
 * @author jakob
 */
public class Actor implements Serializable {

    private final UUID UID = UUID.randomUUID();
    private int height, width;
    private ArrayList<MovementEnum> moves;
    private MovementEnum move;
    private Vector2 nextLocation = new Vector2();
    private Vector2 currentLocation = new Vector2();
    private Vector2 lastLocation = new Vector2();
    private Vector2 target = new Vector2();
    private boolean hasNewTarget = false;
    private float speed;
    private Weapon weapon;
    private ArrayList<Projectile> projectiles;
    private ArrayList<Vector2> path;
    private FileHandle f;
    String sprite;
    public int health;
    private boolean isMoving = false;

    public Actor() {

    }

    public Actor(Vector2 location, int height, int width, String sprite) {

        this.currentLocation = location;
        this.nextLocation = location;
        this.lastLocation = location;
        this.height = height;
        this.width = width;
        this.moves = new ArrayList();
        this.projectiles = new ArrayList<Projectile>();
        this.sprite = sprite;
        this.speed = 0.5f;
        this.move = MovementEnum.STANDING;
        this.path = new ArrayList<Vector2>();
        this.health=100;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public boolean isHasNewTarget() {
        return hasNewTarget;
    }

    public void setHasNewTarget(boolean hasNewTarget) {
        this.hasNewTarget = hasNewTarget;
    }

    public ArrayList<Vector2> getPath() {
        return path;
    }

    public void setPath(ArrayList<Vector2> path) {
        this.path = path;
    }
    
//    public Vector2 tileToPixel(int[][]){
//        new Vector2(int[0][0]*128,int[][])
//    }

    public Vector2 getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(Vector2 currentLocation) {
        this.currentLocation = currentLocation;
    }

    public boolean isIsMoving() {
        return isMoving;
    }

    public void setIsMoving(boolean isMoving) {
        this.isMoving = isMoving;
    }

    public MovementEnum getMove() {
        return move;
    }

    public void setMove(MovementEnum move) {
        this.move = move;
    }

    public String getSprite() {
        return sprite;
    }

    public ArrayList<Projectile> getProjectiles() {
        return projectiles;
    }

    public void setProjectiles(ArrayList<Projectile> projectiles) {
        this.projectiles = projectiles;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public UUID getUID() {
        return UID;
    }

    public Vector2 getNextLocation() {
        return nextLocation;
    }

    public void setNextLocation(Vector2 location) {
        this.nextLocation = location;
    }

    public Vector2 getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(Vector2 lastLocation) {
        this.lastLocation = lastLocation;
    }

    public Vector2 getTarget() {
        return target;
    }

    public void setTarget(Vector2 target) {
        this.target = target;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void setSprite(String sprite) {
        this.sprite = sprite;
    }
 

}
