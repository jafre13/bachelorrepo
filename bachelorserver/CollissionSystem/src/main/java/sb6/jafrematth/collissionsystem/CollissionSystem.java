/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.collissionsystem;

import com.badlogic.gdx.math.Vector2;
import com.hazelcast.core.IMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.openide.util.lookup.ServiceProvider;
import sb6.jafrematth.servercommon.api.ICollission;
import sb6.jafrematth.sharedlibrary.entities.Actor;
import sb6.jafrematth.sharedlibrary.projectiles.Projectile;

/**
 *
 * @author jakob
 */
@ServiceProvider(service = ICollission.class)
public class CollissionSystem implements ICollission {

    HashMap<Vector2, Projectile> hits = new HashMap<>();

    @Override
    public void checkCollission(int[][] gridmap, IMap<UUID, Actor> map) {
        IMap<UUID, Actor> ent = map;
        int[][] gridMap = populateMap(gridmap, map);
        for (IMap.Entry<UUID, Actor> a : ent.entrySet()) {
            a.getValue().setHealth(checkHits(a.getValue()));

            for (int i = a.getValue().getProjectiles().size() - 1; i >= 0; i--) {

                Projectile p = a.getValue().getProjectiles().get(i);
                int x = Math.round(p.getLocation().x);
                int y = Math.round(p.getLocation().y);
                int px = Math.round(a.getValue().getCurrentLocation().x);
                int py = Math.round(a.getValue().getCurrentLocation().y);

                if (x == px && y == py) {
                    continue;
                }
                try{
                if (gridmap[x][y] == 1) {
                    p.setClean(true);
                }

                if (gridmap[x][y] == 10) {
                    hits.put(new Vector2(x, y), p);
                    p.setClean(true);

                }
                }
                catch (Exception e) {
                    
                }
            }

            map.set(a.getKey(), a.getValue());

        }

    }

    public int checkHits(Actor a) {
        Actor t = a;
        int px = Math.round(a.getCurrentLocation().x);
        int py = Math.round(a.getCurrentLocation().y);

        for (Map.Entry<Vector2, Projectile> v : hits.entrySet()) {
            int x = Math.round(v.getKey().x);
            int y = Math.round(v.getKey().y);
            int health = a.getHealth();

            if (x == px && y == py) {

                a.setHealth(health - v.getValue().getDmg());
                v.getValue().setClean(true);
                hits.remove(v.getKey());
            }
        }
        
        return a.getHealth();
    }

    public int[][] populateMap(int[][] gridmap, IMap<UUID, Actor> map) {
        IMap<UUID, Actor> ent = map;
        int[][] temp = gridmap;
        for (IMap.Entry<UUID, Actor> a : ent.entrySet()) {
            int x = Math.round(a.getValue().getCurrentLocation().x);
            int y = Math.round(a.getValue().getCurrentLocation().y);
            temp[x][y] = 10;

        }
        return temp;
    }
}
