/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.weapon;

import com.badlogic.gdx.math.Vector2;
import com.hazelcast.core.IMap;
import java.util.UUID;
import org.openide.util.lookup.ServiceProvider;
import sb6.jafrematth.servercommon.api.ISystem;
import sb6.jafrematth.sharedlibrary.entities.Actor;
import sb6.jafrematth.sharedlibrary.projectiles.Projectile;

/**
 *
 * @author jakob
 *
 */
@ServiceProvider(service = ISystem.class)
public class WeaponSystem implements ISystem {

    @Override
    public void process(IMap<UUID, Actor> map, float delta) {
        float dx = 0, dy = 0;
        for (IMap.Entry<UUID, Actor> actor : map.entrySet()) {
            Actor a = actor.getValue();

            a.getWeapon().getActions().clear();
            for (Projectile p : a.getProjectiles()) {

                Vector2 currentLoc = p.getLocation();
                Vector2 nextLocation = p.getOffSet();
                float dist = currentLoc.dst(nextLocation);
                if (dist > 0) {
                    dx = (nextLocation.x - currentLoc.x) / dist;
                    dy = (nextLocation.y - currentLoc.y) / dist;
                } else {
                    dx = 0;
                    dy = 0;
                }
                float x = currentLoc.x + (dx * p.getSpeed() * delta)/128;
                float y = currentLoc.y + (dy * p.getSpeed() * delta)/128;

//            Vector2 temp = new Vector2(x+(dx * a.getSpeed()*(1/delta)),y+( dy*a.getSpeed()*(1/delta)));
                Vector2 temp = new Vector2(x, y);
//                Vector2 loc=  p.getLocation();
//                Vector2 target = p.getOffSet();
//                float dist = loc.dst(target);
//
//                float dx = target.x - loc.x;
//                float dy = target.y - loc.y;
//                float x = loc.x;
//                float y = loc.y;
//
//                Vector2 temp = new Vector2(x + (dx * a.getSpeed() * (1 / delta))/dist, y + (dy * a.getSpeed() * (1 / delta))/dist);
                p.setLocation(temp);
            }
            map.set(a.getUID(), a);
        }
    }

}
