/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.cleanup;

import com.hazelcast.core.IMap;
import java.util.Iterator;
import java.util.UUID;
import org.openide.util.lookup.ServiceProvider;
import sb6.jafrematth.servercommon.api.ISystem;
import sb6.jafrematth.sharedlibrary.entities.Actor;
import sb6.jafrematth.sharedlibrary.projectiles.Projectile;

/**
 *
 * @author jakob
 */
@ServiceProvider(service = ISystem.class)
public class CleanUpSystem implements ISystem {

    @Override
    public void process(IMap<UUID, Actor> map, float delta) {
        IMap<UUID, Actor> ent = map;
        for (IMap.Entry<UUID, Actor> a : ent.entrySet()) {
            for (int i = a.getValue().getProjectiles().size() - 1; i >= 0; i--) {
                Projectile p = a.getValue().getProjectiles().get(i);
                if (p.isClean()) {
                    a.getValue().getProjectiles().remove(p);
                }
                if (p.getLocation().epsilonEquals(p.getTarget(), 0.01f) || p.getLocation().epsilonEquals(p.getOffSet(), 0.01f)) {
                    a.getValue().getProjectiles().remove(p);
                }

            }
            
            map.set(a.getKey(), a.getValue());
            if (a.getValue().getHealth() <= 0) {
                map.delete(a.getKey());
            }
        }

    }

}
