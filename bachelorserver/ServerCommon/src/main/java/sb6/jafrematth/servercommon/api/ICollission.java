/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.servercommon.api;

import com.hazelcast.core.IMap;
import java.util.UUID;
import sb6.jafrematth.sharedlibrary.entities.Actor;

/**
 *
 * @author jakob
 */
public interface ICollission {
    public void checkCollission(int[][] gridmap, IMap<UUID,Actor> map);
}
