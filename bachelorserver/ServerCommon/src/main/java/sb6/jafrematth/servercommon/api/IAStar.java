/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.servercommon.api;

import com.badlogic.gdx.math.Vector2;
import java.util.ArrayList;

/**
 *
 * @author jakob
 */
public interface IAStar {
    
    ArrayList<Vector2> makePath(Vector2 to, Vector2 from, int[][] gridMap);
    
}
