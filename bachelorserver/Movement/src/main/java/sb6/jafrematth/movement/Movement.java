/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.movement;

import com.badlogic.gdx.math.Vector2;
import com.hazelcast.core.IMap;
import java.util.UUID;
import org.openide.util.lookup.ServiceProvider;
import sb6.jafrematth.servercommon.api.ISystem;
import sb6.jafrematth.sharedlibrary.entities.Actor;

/**
 *
 * @author jakob
 */
@ServiceProvider(service = ISystem.class)
public class Movement implements ISystem {

    int verticalMove = 64;
    int horizontalMove = 128;

    @Override
    public void process(IMap<UUID, Actor> map, float delta) {
        float dx = 0, dy = 0;
        for (IMap.Entry<UUID, Actor> actor : map.entrySet()) {
            Actor a = actor.getValue();
            Vector2 currentLoc = a.getCurrentLocation();
            Vector2 nextLocation = a.getNextLocation();
            float dist = currentLoc.dst(nextLocation);
            if (dist > 0) {
                dx = (nextLocation.x - currentLoc.x) / dist;
                dy = (nextLocation.y - currentLoc.y) / dist;
            } else {
                dx = 0;
                dy = 0;
            }
            float x = currentLoc.x + (dx * a.getSpeed() * delta)/128;
            float y = currentLoc.y + (dy * a.getSpeed() * delta)/128;

            Vector2 temp = new Vector2(x, y);
            a.setCurrentLocation(temp);

            if(currentLoc.epsilonEquals(nextLocation, 0.008f)){
                a.setCurrentLocation(nextLocation);
            }
            
            try {
                if (!a.getPath().isEmpty() && a.getPath() != null) {

                    if (currentLoc.epsilonEquals(nextLocation, 0.2f)) {
                        Vector2 nextLoc = a.getPath().get(0);
                        a.setLastLocation(nextLoc);
                        a.setNextLocation(nextLoc);
                        a.getPath().remove(0);
                    }

                }

            } catch (Exception e) {
            }

            map.set(a.getUID(), a);

        }
    }
}
