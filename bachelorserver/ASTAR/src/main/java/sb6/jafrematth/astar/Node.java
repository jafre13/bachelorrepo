/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.astar;

import com.badlogic.gdx.math.Vector2;


/**
 *
 * @author jakob
 */
public class Node {

    Node parent;
    Vector2 position = new Vector2();
    public float GScore;
    public float HScore;

    public Node(Vector2 pos) {
        this.position = pos;
    }

}
