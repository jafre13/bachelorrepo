/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.astar;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import org.openide.util.lookup.ServiceProvider;
import sb6.jafrematth.servercommon.api.IAStar;

/**
 *
 * @author jakob
 */
@ServiceProvider(service = IAStar.class)
public class ASTAR implements IAStar {

    private int[][] gridMap;

    Map<Vector2, Node> closedList = new HashMap<>();
    Map<Vector2, Node> openChecker = new HashMap<>();
    ArrayList<Vector2> path = new ArrayList<>();
    Node currentNode;

    float cost = 64;
    float dCost = 128;
    PriorityQueue<Node> openList = new PriorityQueue(10, new Comparator<Node>() {
        @Override
        public int compare(Node node1, Node node2) {
            float node1Score = node1.GScore + node1.HScore;
            float node2Score = node2.GScore + node2.HScore;

            if (node1Score < node2Score) {
                return -1;
            } else if (node1Score == node2Score) {
                return 0;
            } else {
                return 1;
            }
        }
    });

    public ASTAR() {

    }

    @Override
    public ArrayList<Vector2> makePath(Vector2 start, Vector2 target, int[][] gridMap) {
        this.gridMap = gridMap;
        openList.clear();
        closedList.clear();
        path.clear();
        openChecker.clear();

        currentNode = new Node(start);
        currentNode.HScore = getH(start, target);
        currentNode.GScore = getG(start, start);
        openList.add(currentNode);

        while (!openList.isEmpty()) {
            currentNode = openList.poll();
            closedList.put(currentNode.position, currentNode);

            if (closedList.containsKey(target)) {
                return finalizePath(currentNode);

            }
            for (Node n : getNeighbours(currentNode)) {
                n.parent = currentNode;
                n.HScore = getH(n.position, target);
                n.GScore = getG(n.position, start);
                openList.add(n);
                openChecker.put(n.position, n);

            }
        }
        return null;
    }

    public float getH(Vector2 from, Vector2 to) {
        float dx = Math.abs(from.x - to.x);
        float dy = Math.abs(from.y - to.y);
        return cost * (dx + dy) + (dCost - 2 * cost) * Math.min(dx, dy);
    }

    public float getG(Vector2 from, Vector2 to) {
        return Math.abs(from.x - to.x) + Math.abs(from.y - to.y);
    }

    public ArrayList<Node> getNeighbours(Node node) {

        ArrayList<Node> neighbours = new ArrayList<>();
        Vector2 pos = node.position;
        int x = (int) pos.x;
        int y = (int) pos.y;

        //1
        if (gridMap[x - 1][y + 1] == 0) {
            if (gridMap[x][y + 1] != 1 || gridMap[x - 1][y] != 1) {
                neighbours.add(new Node(new Vector2(x - 1, y + 1)));
            }
        }
        //2
        if (gridMap[x][y + 1] == 0) {
            neighbours.add(new Node(new Vector2(x, y + 1)));
        }
        //3
        if (gridMap[x + 1][y + 1] == 0) {
            if (gridMap[x][y + 1] != 1 || gridMap[x + 1][y] != 1) {
                neighbours.add(new Node(new Vector2(x + 1, y + 1)));
            }
        }
        //4
        if (gridMap[x - 1][y] == 0) {
            neighbours.add(new Node(new Vector2(x - 1, y)));
        }
        //6
        if (gridMap[x + 1][y] == 0) {
            neighbours.add(new Node(new Vector2(x + 1, y)));
        }//7
        if (gridMap[x - 1][y - 1] == 0) {
            if (gridMap[x - 1][y] != 1 || gridMap[x][y - 1] != 1) {
                neighbours.add(new Node(new Vector2(x - 1, y - 1)));
            }
        }//8
        if (gridMap[x][y - 1] == 0) {
            neighbours.add(new Node(new Vector2(x, y - 1)));
        }
        //9
        if (gridMap[x + 1][y - 1] == 0) {
            if (gridMap[x][y-1] != 1 || gridMap[x+1][y] != 1) {
                neighbours.add(new Node(new Vector2(x + 1, y - 1)));
            }
        }

        ArrayList<Node> finished = new ArrayList<>();
        for (Node neighbourgh : neighbours) {

            if (!closedList.containsKey(neighbourgh.position) && !openChecker.containsKey(neighbourgh.position) && gridMap[x][y] == 0) {
                finished.add(neighbourgh);
            }
        }
        return finished;

    }

    private ArrayList<Vector2> finalizePath(Node n) {
        Node temp = n;
        path.add(temp.position);
        while (temp.parent != null) {
            path.add(temp.parent.position);
            temp = temp.parent;
        }
        Collections.reverse(path);
        return path;
    }

}
