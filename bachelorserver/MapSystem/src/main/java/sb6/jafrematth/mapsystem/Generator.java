/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.mapsystem;

import java.util.Arrays;
import java.util.Random;
import org.openide.util.lookup.ServiceProvider;
import sb6.jafrematth.servercommon.api.IMapSystem;

/**
 *
 * @author jakob
 */
@ServiceProvider(service = IMapSystem.class)
public class Generator implements IMapSystem {

    static int[][] map;

    @Override
    public int[][] generateMap(int sizeX, int sizeY) {
        map = new int[sizeX][sizeY];
        for (int[] a : map) {
            Arrays.fill(a, 0);
        }
        Random rand = new Random();
        for (int i = 0; i < sizeX; i++) {
            for (int j = sizeY - 1; j >= 0; j--) {
                double t = rand.nextDouble();
                //SPAWN POINT ? temporary
                if (i == 0) {
                    map[i][j] = 1;
                }
                if (j == 0) {
                    map[i][j] = 1;

                }
                if (i == sizeX - 1) {
                    map[i][j] = 1;

                }
                if (j == sizeY - 1) {
                    map[i][j] = 1;

                }
                if (t < 0.2) {
                    map[i][j] = 1;

                }
                if (i == 2 && j == 3) {
                    map[i][j] = 0;

                }

            }
        }
        return map;
    }

    public static void main(String[] args) {
        Generator gen = new Generator();
        gen.generateMap(100, 100);

        for (int[] b : map) {
            for (int a : b) {
                if (a == 1) {
                    System.out.printf(" X ");
                } else if (a == 0) {
                    System.out.printf(" O ");
                }
            }
            System.out.println(" ");
        }

    }

}
