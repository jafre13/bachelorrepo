/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.servercore;

import com.badlogic.gdx.math.Vector2;
import com.hazelcast.config.*;
import com.hazelcast.core.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;

import org.openide.modules.ModuleInstall;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import sb6.jafrematth.servercommon.api.IAStar;
import sb6.jafrematth.servercommon.api.ICollission;
import sb6.jafrematth.servercommon.api.IMapSystem;
import sb6.jafrematth.servercommon.api.ISystem;
import sb6.jafrematth.sharedlibrary.entities.Actor;
import sb6.jafrematth.sharedlibrary.maps.GameLevel;

public class Installer extends ModuleInstall {

    float start = 0, diff, wait;
    Thread t;
    boolean running = true;
    public IMap<String, GameLevel> maps;
    public IMap<UUID, Actor> players;
    HazelcastInstance instance;
    private final Lookup lookup = Lookup.getDefault();
    ArrayList<ISystem> systems;
    IMapSystem mapSystem;
    IAStar ai;
    ICollission col;

    Scanner sc;

    long lastTime = System.nanoTime(), current = 0, delta = 0;

    @Override
    public void restored() {
        Config conf = new Config();

//        conf.getNetworkConfig().setPort(4545);
//        conf.getNetworkConfig().setPortAutoIncrement(false);
//        conf.getNetworkConfig().getJoin().getMulticastConfig().setEnabled(false);
//        conf.getNetworkConfig().getJoin().getTcpIpConfig().addMember("10.126.135.173").
//        addMember("10.137.0.108").addMember("130.225.157.112").setEnabled(true);
//        conf.getNetworkConfig().getInterfaces().setEnabled(true).addInterface("10.126.135.173").
//        addInterface("10.137.0.108").addInterface("130.225.157.112");
//        conf.getNetworkConfig().addOutboundPort(4545);
//        conf.getNetworkConfig().addOutboundPortDefinition("4545");
//        conf.getNetworkConfig().setPublicAddress("130.225.157.112");
        instance = Hazelcast.newHazelcastInstance(conf);
        maps = instance.getMap("maps");
        players = instance.getMap("players");

//        entities = instance.getMap("actors");
        GameLevel test = new GameLevel("test", "jakob2.tmx", 20, 20);
        Lookup.Result<IMapSystem> result = lookup.lookupResult(IMapSystem.class);
        result.allItems();
        
        
        for (IMapSystem a : result.allInstances()) {
            mapSystem = a;
        }
        test.objects = mapSystem.generateMap(test.height, test.width);
        maps.set(test.name, test);

        Lookup.Result<ISystem> result2 = lookup.lookupResult(ISystem.class);
        systems = new ArrayList<>(result2.allInstances());
        result2.allItems();

        for (IAStar a : lookup.lookupResult(IAStar.class).allInstances()) {
            ai = a;
        }
        for (ICollission c : lookup.lookupResult(ICollission.class).allInstances()) {
            col = c;
        }

        try {
            update();
        } catch (InterruptedException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public void update() throws InterruptedException {
        while (running) {

            getDelta();

            for (ISystem s : systems) {
                for (IMap.Entry<String, GameLevel> map : maps.entrySet()) {
                    s.process(players, delta);

                    for (IMap.Entry<UUID, Actor> a : players.entrySet()) {
                        Actor actor = a.getValue();
                        if (actor.isHasNewTarget()) {
                            ArrayList<Vector2> path = new ArrayList<>();
                            path = ai.makePath(actor.getNextLocation(), actor.getTarget(),
                                    map.getValue().getObjects());
                            if (path != null) {
                                actor.setPath(path);
                                actor.setHasNewTarget(false);
                            }
                        }
                        players.set(actor.getUID(), actor);

                    }
                }
            }
            for (IMap.Entry<String, GameLevel> map : maps.entrySet()) {
                col.checkCollission(map.getValue().objects, players);
            }

        }
    }

    private void getDelta() {

        current = System.nanoTime();
        delta = (current - lastTime) / 1000000;
        lastTime = current;

    }

}
