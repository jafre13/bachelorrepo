/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.core.game.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import java.util.ArrayList;
import java.util.UUID;
import org.openide.util.Lookup;
import sb6.jafrematth.common.api.ISystem;

import sb6.jafrematth.core.game.HellDude;
import sb6.jafrematth.sharedlibrary.entities.Actor;
import sb6.jafrematth.sharedlibrary.helpermethods.SpriteManager;
import sb6.jafrematth.sharedlibrary.projectiles.Projectile;

import sb6.jafrematth.sharelibrary.weapons.FireStaff;

/**
 *
 * @author jakob
 */
public class KillFloorScreen implements Screen {

    Vector3 pos;

    public final float TILE_WIDTH_HALF = 64f;
    public final int TILE_HEIGHT_HALF = 32;
    public final int TILE_WIDTH = 128;
    public float TILE_DIAG = (float) (Math.sqrt((TILE_WIDTH_HALF * TILE_WIDTH_HALF) + ((TILE_WIDTH_HALF) * (TILE_WIDTH_HALF))));

    private Texture textureTileset = null;
    private TextureRegion[] tileSet = null;

    int mapWidth;
    int mapHeight;
    Actor player;
    UUID uid;
    long now, shotTimer;

    final HellDude hd;
    OrthographicCamera cam;
    ArrayList<ISystem> systems;
    int[][] walls;

    private final Lookup lookup = Lookup.getDefault();

    Texture tex;
//    Sprite sprite;

    private Matrix4 isoTransform = null;
    private Matrix4 invIsotransform = null;
    private Matrix4 id = null;

    private int count = 0;
    BitmapFont font;
    HazelcastInstance client;

    public KillFloorScreen(final HellDude game) {

        //Basic setup
        font = new BitmapFont();
        font.setColor(Color.CYAN);

        pos = new Vector3();
        Lookup.Result<ISystem> result = lookup.lookupResult(ISystem.class);
        systems = new ArrayList<>(result.allInstances());
        result.allItems();
        hd = game;

//Map setup
        id = new Matrix4();
        id.idt();

        //create the isometric transform
        isoTransform = new Matrix4();
        isoTransform.idt();
        isoTransform.translate(0f, TILE_WIDTH_HALF / 2, 0.0f);

        isoTransform.scale(TILE_DIAG, TILE_DIAG / 2, 1f);

        isoTransform.rotate(0.0f, 0.0f, 1.0f, -45f);

        //... and the inverse matrix
        invIsotransform = new Matrix4(isoTransform);
        invIsotransform.inv();

        textureTileset = new Texture(SpriteManager.getSpriteManager().getUrl("assets/maps/TilesetWalls.png"));
        textureTileset.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
        tileSet = new TextureRegion[4];

        tileSet[0] = new TextureRegion(textureTileset, 256, 0, 128, 128);
        tileSet[1] = new TextureRegion(textureTileset, 256, 128, 128, 128);
        //Camera setup
        cam = new OrthographicCamera();
        cam.setToOrtho(false, 1600, 900);

        //Player setup
        player = new Actor(new Vector2((int) 2, (int) 3), 2, 3, "char2.png");
        player.setWeapon(new FireStaff(20, 20, 4, 2000, 5));
        uid = player.getUID();
//        
        //Hazelcast setup
        hd.players.set(player.getUID(), player);

    }

    @Override
    public void render(float delta) {
        //Call the update to check inputs and shit
        update(delta);

        //GDX shit
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        hd.batch.setProjectionMatrix(cam.combined);
        hd.batch.setTransformMatrix(id);
        renderFoundation();

        hd.batch.setTransformMatrix(isoTransform);
        hd.batch.setProjectionMatrix(cam.combined);
        hd.batch.begin();
        renderActors();
        renderProjectiles();
        hd.batch.end();

        hd.sr.setTransformMatrix(isoTransform);
        hd.sr.setProjectionMatrix(cam.combined);
        renderHUD(delta);

    }

    public void renderFoundation() {

        hd.batch.begin();
        for (int x = 0; x < hd.currentMap.width; x++) {
            for (int y = hd.currentMap.height - 1; y >= 0; y--) {

                float x_pos = (x * 128 / 2.0f) + (y * 128 / 2.0f);
                float y_pos = -(x * 64 / 2.0f) + (y * 64 / 2.0f);

                if (hd.currentMap.objects[x][y] == 0) {
                    hd.batch.draw(tileSet[0], x_pos, y_pos, 128, 128);

                }
                if (hd.currentMap.objects[x][y] == 1) {
                    hd.batch.draw(tileSet[1], x_pos, y_pos, 128, 128);

                }

            }
        }
        hd.batch.end();
    }

    public void renderActors() {

        for (IMap.Entry<UUID, Actor> ent : hd.players.entrySet()) {
            Actor value = ent.getValue();
            if (!hd.sprites.containsKey(value.getUID())) {
                hd.sprites.put(value.getUID(), SpriteManager.getSpriteManager().getSprite(value.getSprite()));
            }

            float x = value.getCurrentLocation().x - 1f;
            float y = value.getCurrentLocation().y - .6f;
            hd.batch.draw(hd.sprites.get(value.getUID()), x, y, value.getHeight(), value.getWidth());
        }
    }

    public void renderProjectiles() {

        for (IMap.Entry<UUID, Actor> ent : hd.players.entrySet()) {
            Actor a = ent.getValue();
            for (Projectile p : a.getProjectiles()) {
                if (!hd.projectileSprites.containsKey(p.getTexture())) {
                    hd.projectileSprites.put(p.getTexture(), SpriteManager.getSpriteManager().getSprite(p.getTexture()));
                }
                float x = p.getLocation().x;
                float y = p.getLocation().y;
                hd.batch.draw(hd.projectileSprites.get(p.getTexture()), x, y, p.getRadius(), p.getRadius());
            }
        }

    }

    public void renderHUD(float delta) {
        hd.hudBatch.begin();
        font.draw(hd.hudBatch, pos.toString(), 20, 20);
        font.draw(hd.hudBatch, "" + 1 / delta, 20, 40);
        font.draw(hd.hudBatch, "x: " + (int) pos.x + " y: " + (int) pos.y, 20, 60);
        hd.hudBatch.end();
        hd.sr.begin(ShapeRenderer.ShapeType.Filled);

        for (IMap.Entry<UUID, Actor> a : hd.players.entrySet()) {
            Actor j = a.getValue();
            Vector2 t = j.getCurrentLocation().cpy();
            t.x += 0.1f;
            t.y -= 0.1f;
            Vector2 h = t.cpy();
            Vector2 n = t.cpy();
            n.x += 1;
            n.y += 1;
            float green = j.getHealth();
            float red = 100 - j.getHealth();
            h.x += green / 100;
            h.y += green / 100;
            hd.sr.setColor(Color.GREEN);
            hd.sr.rectLine(t, h, 0.1f);
            hd.sr.setColor(Color.RED);
            hd.sr.rectLine(h, n, 0.1f);
        }
        hd.sr.end();

    }

    public void update(float delta) {
        if (hd.players.size() > 0) {

            if (hd.players.containsKey(uid)) {
                player = hd.players.get(player.getUID());

            } else {
                player = null;
            }

            if (player != null) {
                if (Gdx.input.isKeyJustPressed(Keys.M)) {
                    player.setHealth(0);
                    hd.players.replace(uid, player);

                }
                if (player.getHealth() <= 0) {
                    dispose();
                    hd.setScreen(new LobbySceen(hd, 0));
                }
                
                if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {

                    player.setHasNewTarget(true);
                    player.setTarget(new Vector2((int) pos.x, (int) pos.y));
                }
                if (Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) {
//                player.getWeapon().getActions().add(WeaponEnum.FIRE);
                    now = com.badlogic.gdx.utils.TimeUtils.millis();
                    if (shotTimer <= now) {
                        shotTimer = now + 500;
                        player.getProjectiles().add(new Projectile(player.getCurrentLocation().x, player.getCurrentLocation().y, pos.x, pos.y, 1, 3));
                        hd.players.replace(uid, player);

                    }

                }

                if (Gdx.input.isKeyPressed(Keys.ESCAPE)) {
                    dispose();
                    System.exit(0);
                }

//            
                cam.position.set(player.getCurrentLocation().cpy().lerp(player.getNextLocation(), delta), 0);
                cam.position.mul(isoTransform);
                cam.update();

                hd.players.replace(player.getUID(), player);
            } else {
                hd.setScreen(hd.ls);
            }

        }

        pos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
        cam.unproject(pos);
        pos.mul(invIsotransform);
        pos.x = (int) pos.x;
        pos.y = (int) pos.y;

    }

    @Override
    public void show() {
    }

    @Override
    public void resize(int width, int height) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void pause() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void resume() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void hide() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void dispose() {
//
//        map.dispose();
//        hd.HazelActors.remove(player.getUID());
////        hd.HazelActors.remove(player.getUID());

    }

}
