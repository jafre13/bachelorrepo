/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.core.game.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import sb6.jafrematth.core.game.HellDude;
import sb6.jafrematth.sharedlibrary.helpermethods.SpriteManager;

/**
 *
 * @author jakob
 */
public class LobbySceen implements Screen {

    BitmapFont font = new BitmapFont();
    HellDude hd;
    public int stat;
    String message;
    Texture backTexture;
    
    public LobbySceen(final HellDude game, int action) {
        hd = game;
        message = "Press p to play";
        stat = action;
        font.setColor(Color.WHITE);
        backTexture = SpriteManager.getSpriteManager().getTexture("HellDudePoster.png");
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        font.setColor(Color.WHITE);
        hd.lsBatch.begin();
        hd.lsBatch.draw(backTexture, 0, 0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        if (stat == 0) {
            font.draw(hd.lsBatch, "Welcome to Helldude", Gdx.graphics.getWidth() / 2, (Gdx.graphics.getHeight() / 2));
            font.draw(hd.lsBatch, message, Gdx.graphics.getWidth() / 2, (Gdx.graphics.getHeight() / 2) - 20);
        }
       
        else if (stat == 1) {
            font.draw(hd.lsBatch, "you have died", Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
            font.draw(hd.lsBatch, message + " again", Gdx.graphics.getWidth() / 2, (Gdx.graphics.getHeight() / 2) - 20);
        }
        hd.lsBatch.end();

        if (Gdx.input.isKeyPressed(Keys.P)) {
            hd.setScreen(new KillFloorScreen(hd));
        }
    }

    @Override
    public void resize(int arg0, int arg1) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
    }

}
