/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.core.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.Sprite;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import java.util.HashMap;
import java.util.UUID;
import sb6.jafrematth.core.game.scenes.KillFloorScreen;
import sb6.jafrematth.core.game.scenes.LobbySceen;
import sb6.jafrematth.sharedlibrary.entities.Actor;
import sb6.jafrematth.sharedlibrary.maps.GameLevel;

/**
 *
 * @author jakob
 */
public class HellDude extends Game {

    public HazelcastInstance client;
    public IMap<String, GameLevel> maps;
    public IMap<UUID,Actor> players;
    public HashMap<UUID, Sprite> sprites;
    public HashMap<String, Sprite> projectileSprites;
    public GameLevel currentMap;

    public LobbySceen ls;
    public SpriteBatch batch;
    public SpriteBatch lsBatch;
    public SpriteBatch hudBatch;
    public ShapeRenderer sr;
    boolean running = true;

    public void connect() {

        ClientConfig clientConfig = new ClientConfig();
        clientConfig.getGroupConfig().setName("dev").setPassword("dev-pass");

//        clientConfig.getNetworkConfig().addAddress("130.225.157.112:4545");
        client = HazelcastClient.newHazelcastClient(clientConfig);

        maps = client.getMap("maps");
        currentMap = maps.get("test");
        System.out.println(currentMap.objects);
        players = client.getMap("players");
        

    }

    @Override
    public void create() {
        ls = new LobbySceen(this, 0);
        connect();
        batch = new SpriteBatch();
        lsBatch = new SpriteBatch();
        hudBatch = new SpriteBatch();
        sr = new ShapeRenderer();

        sprites = new HashMap();
        projectileSprites = new HashMap<>();
        this.setScreen(ls);

    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void render() {
        update();
        super.render();
    }

    public void update() {

//        

    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        batch.dispose();
        sr.dispose();

    }

}
