/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb6.jafrematth.core;


import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

import org.openide.modules.ModuleInstall;
import sb6.jafrematth.core.game.HellDude;

public class Installer extends ModuleInstall {

    @Override
    public void restored() {
        
        Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
        config.setTitle("Hell Dude");
        config.setWindowedMode(1600, 900);
        
        new Lwjgl3Application(new HellDude(),config);

    }
}

